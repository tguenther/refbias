#!/usr/bin/env python
#contact: pontus.skoglund@gmail.com

import sys

threshold=float(sys.argv[1])

if len(sys.argv)>2:
	lenthresh=int(sys.argv[2])
else:
	lenthresh=35

lencount=0
qualcutoff=30

for line in sys.stdin:
	if line[0] == '@':
		print line.rstrip('\n')
		continue
	col=line.split('\t')
	readlen=len(col[9])
	try:
		mismatches=int(line.split('NM:i:')[1].split('\t')[0])
		identity=1.0*(readlen-mismatches)/readlen
	except:
		print >>sys.stderr,'No NM field'
		continue
	#MAPQ = int(col[4])
	#print identity,'\t',MAPQ
	if identity > threshold and readlen >= lenthresh:
		print line.rstrip('\n')
	if readlen < lenthresh & int(col[4])>=qualcutoff:
		lencount+=1

if len(sys.argv)>3:
	f=open(sys.argv[3],'w')
	f.write('%s'%lencount)
	f.close()
