#!/bin/bash

module load bioinfo-tools
module load samtools/1.5
module load bwa/0.7.17

inbam=$1
bedfile=$2
bedfilter_dir=/proj/sllstore2017087/nobackup/private/reference_bias/bedfilter_bam3/

samtools view -L $bedfile -h -b -u -q 30 $inbam > ${TMPDIR}/bedfilter.bam
samtools index ${TMPDIR}/bedfilter.bam

samtools calmd ${TMPDIR}/bedfilter.bam /proj/sllstore2017020/private/ref_seqs/hs37d5.fa | ./modify_read_alternative.py SGDP.snplist.txt | samtools view -b -u - > ${TMPDIR}/modified.bam

bwa aln -l 16500 -n 0.01 -o 2 -t 10 /proj/sllstore2017020/private/ref_seqs/hs37d5.fa -b ${TMPDIR}/modified.bam \
| bwa samse /proj/sllstore2017020/private/ref_seqs/hs37d5.fa - ${TMPDIR}/modified.bam  \
| samtools view -F 4 -h - \
| samtools sort -o ${TMPDIR}/modified.remap.bam -

fbase=$(basename $inbam .bam)

samtools index ${TMPDIR}/modified.remap.bam

samtools calmd -b ${TMPDIR}/modified.remap.bam /proj/sllstore2017020/private/ref_seqs/hs37d5.fa > ${TMPDIR}/modified.remap.MD.bam

samtools index ${TMPDIR}/modified.remap.MD.bam

samtools calmd ${TMPDIR}/bedfilter.bam /proj/sllstore2017020/private/ref_seqs/hs37d5.fa | samtools view -L $bedfile -h -q 30 - | ./filter_sam_startpos_dict.py ${TMPDIR}/modified.remap.MD.bam | samtools view -bh - > ${TMPDIR}/remapmodread.filter.bam

samtools index ${TMPDIR}/remapmodread.filter.bam

mv ${TMPDIR}/modified.remap.bam ${bedfilter_dir}/$fbase.modified.remap.bam
mv ${TMPDIR}/modified.remap.bam.bai ${bedfilter_dir}/$fbase.modified.remap.bam.bai

mv ${TMPDIR}/remapmodread.filter.bam ${bedfilter_dir}/$fbase.remapmodread.filter.bam
mv ${TMPDIR}/remapmodread.filter.bam.bai ${bedfilter_dir}/$fbase.remapmodread.filter.bam.bai

mv ${TMPDIR}/modified.remap.posfilter.bam ${bedfilter_dir}/$fbase.modified.remap.posfilter.bam
mv ${TMPDIR}/modified.remap.posfilter.bam.bai ${bedfilter_dir}/$fbase.modified.remap.posfilter.bam.bai

