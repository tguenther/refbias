from Bio import SeqIO
from Bio.SeqRecord import SeqRecord
from random import shuffle
import sys

##generate a pseudogenome with the alternative alleles from the SGDP data
##Run: python modify_refseq_different_allele.py $tpedfile $refseq $bedfile


snpref=sys.argv[1] #'/proj/sllstore2017087/nobackup/private/reference_bias/SGDP.merged.all.tped'

records=list(SeqIO.parse(sys.argv[2],'fasta'))
seq_dic={}
for r in records:
	seq=r.seq
	newseq=seq.tomutable()
	seq_dic[r.id]=newseq


snppos={}
snpid={}

nucs=['A','C','G','T']

posref=sys.argv[3]
pos_dic={}
for l in open(posref):
	split=l.split()
	pos_dic[(split[0],split[2])]=1

found=0

f=open(snpref)
for l in f:
	split=l.split()
	if not pos_dic.has_key((split[0],split[3])):
		continue
	alleles=list(set(split[4:]))
	if '0' in alleles:
		alleles.remove('0')
	if 'N' in alleles:
		alleles.remove('N')
	if len(alleles)!=2:
		continue
	c=split[0]
	p=int(split[3])-1
	if seq_dic[c][p] in alleles:
		nucs=['A','G','C','T']
		shuffle(nucs)
		found+=1
		for n in nucs:
			if n not in alleles:
				seq_dic[c][p]=n
				break
f.close()


for i in xrange(len(records)):
	r=records[i]
	r=SeqRecord(seq_dic[r.id],id=r.id,description='')
	records[i]=r


SeqIO.write(records,"modified_ref.pseudogenome.fa",'fasta')

