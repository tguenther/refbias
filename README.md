# Scripts for filtering BAM files at a known set of SNPs in order to reduce reference bias

Please cite Günther, T. & Nettelblad C. (2019) ‘The presence and impact of reference bias on population genomic studies of prehistoric human populations’ PLOS Genetics https://doi.org/10.1371/journal.pgen.1008302

Contact torsten 'dot' guenther 'at' ebc 'dot' uu 'dot' se for comments.

## (not very efficient) Python 2.7 scripts

**filter_sam_startpos_dict.py** -- Reads a bam file from stdin and compares it to a modified bam file. for each read, the location of original and modified BAM are compared and only reads mapping to the same position are written to stdout. Usage: samtools calmd $originalbam $refseq | samtools view -L $bedfile -h -q 30 - | filter_sam_startpos_dict.py $modifiedbam | samtools view -bh - > $modified.filter.bam

**modify_read_alternative.py** -- Reads a bam file from stdin and modifies reads that overlap with a set of biallelic SNPs to carry the other allele, outputs SAM to stdin; takes a SNPlist (columns: chr, bp, ref_allele, alt_allele) as argument. Usage: samtools calmd $originalbam /proj/sllstore2017020/private/ref_seqs/hs37d5.fa | ./modify_read_alternative.py SGDP.snplist.txt | samtools view -b -u - > modified.bam

**modify_refseq_different_allele.py** -- Generate a pseudogenome with "third" alleles using a set of biallelic SNPs. Usage: python modify_refseq_different_allele.py $tpedfile $refseq $bedfile

## Mapping scripts

**remap_modified_reads.sh** -- Remap reads modified to carry the "other" allele and test if they still map to the same position

**remap_modref.sh** -- Remap reads to a modified version of the reference genome and test if they still map to the same position
