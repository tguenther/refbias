#!/usr/bin/env python

import sys

minthresh=float(sys.argv[1])
maxthresh=float(sys.argv[2])

for line in sys.stdin:
	if line[0] == '@':
		print line.rstrip('\n')
		continue
	col=line.split('\t')
	readlen=len(col[9])
	if minthresh<=readlen and maxthresh>readlen:
		print line.rstrip('\n')
