#!/bin/bash

module load bioinfo-tools
module load samtools/1.5
module load bwa/0.7.17



original=$1
bedfile=$2


bedfilter_dir=/proj/sllstore2017087/nobackup/private/reference_bias/bedfilter_bam3/

cp $original ${TMPDIR}/orig.bam
cp ${original}.bai ${TMPDIR}/orig.bam.bai

samtools calmd ${TMPDIR}/orig.bam /proj/sllstore2017020/private/ref_seqs/hs37d5.fa | samtools view -L $bedfile -h -b -q 30 - > ${TMPDIR}/bedfilter.bam
samtools index ${TMPDIR}/bedfilter.bam

bwa aln -l 16500 -n 0.01 -o 2 -t 8 ref_seqs/modified_ref.pseudogenome.fa -b ${TMPDIR}/bedfilter.bam \
| bwa samse ref_seqs/modified_ref.pseudogenome.fa - ${TMPDIR}/bedfilter.bam  \
| samtools view -F 4 -h - \
| samtools sort -o ${TMPDIR}/bedfilter.altref.bam -

samtools index ${TMPDIR}/bedfilter.altref.bam

samtools calmd -b ${TMPDIR}/bedfilter.altref.bam ref_seqs/modified_ref.pseudogenome.fa > ${TMPDIR}/bedfilter.altref.MD.bam

samtools index ${TMPDIR}/bedfilter.altref.MD.bam

samtools calmd ${TMPDIR}/bedfilter.bam /proj/sllstore2017020/private/ref_seqs/hs37d5.fa | samtools view -L $bedfile -h -q 30 - | ./filter_sam_startpos_dict.py ${TMPDIR}/bedfilter.altref.MD.bam | samtools view -bh - > ${TMPDIR}/bedfilter.posfilter.altref.bam


samtools index ${TMPDIR}/bedfilter.posfilter.altref.bam

fbase=$(basename $original .bam)

for fb in bedfilter bedfilter.altref bedfilter.posfilter.altref
do
mv ${TMPDIR}/$fb.bam ${bedfilter_dir}/$fbase.$fb.bam
mv ${TMPDIR}/$fb.bam.bai ${bedfilter_dir}/$fbase.$fb.bam.bai
done

